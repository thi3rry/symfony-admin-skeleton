# Symfony Skeleton

Initialisé le 01/10/2020 sous Symfony 5

Application symfony d'administration des adhérents FSB 

## Fonctionnalités

- EasyAdmin
- User & AdminUser
- StaticPage controller, ajouter juste des templates dans `templates/static` et il sera visible à l'url `/static/{slug}`.
  Ex:
  ```twig
  {# templates/static/getting-started.html.twig #}
  {% extends 'base.html.twig' %}
  
  {% block body %}
  <h1>Démarrage rapide</h1>
  {% endblock body %}
  ```
  Affichera une page avec un titre H1 sur l'url http://localhost:8000/static/getting-started
- Page Controller : Une entité simple Page (titre, slug, content) affiche les pages sur l'url `http://localhost:8000/page/{slug}`


## Pré-requis

PHP 7.4
MySQL 5.5 ou MariaDB 10.4
Composer

## Installation

Install `hirak/prestissimo` to speed up composer downloads

```shell script
composer global require "hirak/prestissimo:^0.3"
```

```shell script
yarn install
composer install
php bin/console doctrine:migrations:migrate
php bin/console assets:install --symlink --relative
```

### Première installation

Créer l'utilisateur admin `contact@shoot-shoot.com` avec le mot de passe `admin` : 
```shell script
php bin/console doctrine:query:sql "INSERT INTO admin_user (email, roles, password, created_at, updated_at )
VALUES ('contact@shoot-shoot.com', '[\"ROLE_SUPER_ADMIN\"]', '\$argon2id\$v=19\$m=65536,t=4,p=1\$T5f0MespPy63uzZslYiFFA\$AE/cXJJftcEaFOOOB3I0gNHinsZBZOvftZB0ADA+WqE', '2020-07-09 10:43:00', '2020-07-09 10:43:00');"
```


## Développement

Démarrage serveur web : 

```shell script
php -S localhost:8000 -t public
```

Goto : http://localhost:8000

Watch des assets : 
```shell script
yarn run dev --watch
```

## Bugs connus

### Problème de transchoice avec EasyAdmin 3 et Symfony > 4.1

- https://github.com/EasyCorp/EasyAdminBundle/pull/2479/files
- https://github.com/symfony/symfony-docs/issues/11002

Les templates suivants ont été mis à jour pour ne plus avoir de transchoice : 
- templates/bundles/EasyAdminBundle/crud/form_theme.html.twig
- templates/bundles/EasyAdminBundle/crud/paginator.html.twig
- templates/bundles/EasyAdminBundle/exception.html.twig

## Resources

- Mise en place des token JWT : https://www.attineos.com/blog/tech/tutotrompe-video-symfony
- Customization des token JWT : https://github.com/lexik/LexikJWTAuthenticationBundle/blob/master/Resources/doc/2-data-customization.md
- Doctrine Extensions : 
    - https://github.com/Atlantic18/DoctrineExtensions/tree/v2.4.x/doc
    - https://github.com/Atlantic18/DoctrineExtensions
- EasyAdmin bundles: 
    - Dashboard : https://symfony.com/doc/current/bundles/EasyAdminBundle/dashboards.html
    - Actions : https://symfony.com/doc/master/bundles/EasyAdminBundle/actions.html
    - Fields : https://symfony.com/doc/master/bundles/EasyAdminBundle/fields.html
- JMS Serializer Annotations reference : http://jmsyst.com/libs/serializer/master/reference/annotations
- php-translation bundle : https://php-translation.readthedocs.io/en/latest/symfony/profiler-ui.html
- Utilisation d'un type spécifique sur une propriété pour utiliser un service avec un handler pour sérialiser cette propriété : https://stackoverflow.com/questions/43932220/symfony-jmsserializer-accessor-call-service-function

