<?php

namespace App\Entity;

use App\Repository\User\UserRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface as BaseUserInterface;

interface UserInterface extends BaseUserInterface
{
    public function hasRole($role) : bool;
    public function addRole($role) : self;
    public function removeRole($role) : self;
    public function getPlainPassword(): string;
    public function setPlainPassword(string $plainPassword): self;
    public function setPassword(string $password): self;
}
