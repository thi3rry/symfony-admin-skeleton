<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class StaticPageController extends AbstractController
{
    /**
     * @Route("/static/{slug}", name="static_page_with_slug")
     */
    public function page($slug)
    {
        if(file_exists(dirname(dirname(__DIR__)).'/templates/static/'.$slug.'.html.twig')) {
            return $this->render('static/'.$slug.'.html.twig', [
                'slug' => $slug,
            ]);
        }
        return $this->createNotFoundException();
    }
}
