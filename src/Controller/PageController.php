<?php

namespace App\Controller;

use App\Entity\Page;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class PageController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     */
    public function homepage(EntityManagerInterface $em)
    {
        $pages = [];
        try {
            $pages = $em->getRepository(Page::class)->findBy([]);
        }
        catch (\Exception $e) {
            $pages = [];
        }

        return $this->render('page/homepage.html.twig', [
            'pages' => $pages
        ]);
    }

    /**
     * @Route("/page/{slug}", name="page_with_slug")
     */
    public function show(Page $page)
    {
        return $this->render('page/show.html.twig', [
            'page' => $page,
        ]);
    }
}
