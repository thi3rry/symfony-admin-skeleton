<?php

namespace App\Controller\Admin;

use App\Entity\Page;
use App\Entity\User\AdminUser;
use App\Entity\User\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Assets;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\Menu\SectionMenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Gedmo\Loggable\Entity\LogEntry;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * Class DashboardController
 * @package App\Controller\Admin
 * @IsGranted("ROLE_ADMIN")
 */
class DashboardController extends AbstractDashboardController
{

    public function configureAssets(): Assets
    {
        return Assets::new()
            ->addCssFile('field-boolean.css')
        ;
    }

    /**
     * @Route("/admin", name="admin_dashboard")
     */
    public function index(): Response
    {
        return parent::index();
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('ADMIN');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoDashboard('Dashboard', 'fas fa-home');

        yield MenuItem::section('Utilisateurs', 'fas fa-user');
        yield MenuItem::linkToCrud('Utilisateurs', 'fas fa-user', User::class);
        yield MenuItem::linkToCrud('Admins', 'fas fa-user-shield', AdminUser::class)
                ->setPermission('ROLE_SUPER_ADMIN');

        yield MenuItem::section('Administration', '')
            ->setPermission('ROLE_SUPER_ADMIN');
        yield MenuItem::linkToCrud('Admins', 'fas fa-user-shield', AdminUser::class)
            ->setPermission('ROLE_SUPER_ADMIN');

        yield MenuItem::section('Contenus', '')
            ->setPermission('ROLE_ADMIN');

        yield MenuItem::linkToCrud('Pages', '', Page::class)
            ->setPermission('ROLE_PAGE_EDITOR');
//        yield MenuItem::linkToCrud('Logs', 'fas fa-clipboard-list', LogEntry::class)
//            ->setPermission('ROLE_SUPER_ADMIN');

    }
}
