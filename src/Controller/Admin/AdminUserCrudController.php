<?php

namespace App\Controller\Admin;

use App\Entity\User\AdminUser;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class AdminUserCrudController
 * @package App\Controller\Admin
 * @IsGranted("ROLE_SUPER_ADMIN")
 */
class AdminUserCrudController extends UserCrudController
{


    public static function getEntityFqcn(): string
    {
        return AdminUser::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            EmailField::new('email'),
            TextField::new('plainPassword')
                ->setFormType(PasswordType::class)
                ->setLabel('Password')
                ->hideOnIndex(),
            BooleanField::new('isSuperAdmin')
                ->setLabel('SuperAdmin'),
        ];
    }

}
