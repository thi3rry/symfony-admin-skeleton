<?php

namespace App\Controller\Admin;

use App\Entity\User\AdminUser;
use App\Entity\User\Group;
use App\Entity\User\User;
use App\Entity\UserInterface;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Assets;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class UserCrudController
 * @package App\Controller\Admin
 * @IsGranted("ROLE_ADMIN")
 */
class UserCrudController extends AbstractCrudController
{

    /** @var UserPasswordEncoderInterface */
    protected $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public static function getEntityFqcn(): string
    {
        return User::class;
    }


    public function configureFields(string $pageName): iterable
    {
        $fields = [
            BooleanField::new('blocked')
                ->addCssClass('field-boolean-danger'),
            EmailField::new('email'),
            TextField::new('plainPassword')
                ->onlyOnForms()
                ->setFormType(PasswordType::class)
                ->setLabel('Password')
                ->setFormTypeOption('empty_data', '')
                ->setRequired(false),
            TextField::new('firstname'),
            TextField::new('lastname'),
            TextEditorField::new('adminNotes')
                ->onlyOnForms(),
        ];

        return $fields;
    }

    public function persistEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        /** @var UserInterface $entityInstance */
        $entityInstance->setPassword(
            $this->encoder->encodePassword($entityInstance, $entityInstance->getPlainPassword())
        );
        parent::persistEntity($entityManager, $entityInstance);
    }

    public function updateEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $oldPasswordHashed = $entityInstance->getPassword();
        if (!empty($entityInstance->getPlainPassword())) {
            $newPasswordHashed = $this->encoder->encodePassword($entityInstance, $entityInstance->getPlainPassword());
            if ($newPasswordHashed !== $oldPasswordHashed) {
                /** @var UserInterface $entityInstance */
                $entityInstance->setPassword($newPasswordHashed);
            }
        }
//        else {
//            $this->addFlash('warning', "Le mot de passe n'a pas été modifié");
//        }
//        $this->addFlash('success', "L'utilisateur a été sauvagardé");
        parent::updateEntity($entityManager, $entityInstance);
    }
}
